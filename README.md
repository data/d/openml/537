# OpenML dataset: houses

https://www.openml.org/d/537

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:  Kelley Pace

**Source**: http://lib.stat.cmu.edu/datasets/

**Please cite**:   

If you use an algorithm, dataset, or other information from StatLib, please acknowledge both StatLib and the original contributor of the material. 

Pace and Barry (1997), "Sparse Spatial Autoregressions", Statistics and Probability Letters.

@article{pace1997sparse,
  title={Sparse spatial autoregressions},
  author={Pace, R Kelley and Barry, Ronald},
  journal={Statistics \& Probability Letters},
  volume={33},
  number={3},
  pages={291--297},
  year={1997},
  publisher={Elsevier}
}

**Data description**

S&P Letters Data
We collected information on the variables using all the block groups in California from the 1990 Census. In this sample a block group on average includes 1425.5 individuals living in a geographically compact area. Naturally, the geographical area included varies inversely with the population density. We computed distances among the centroids of each block group as measured in latitude and longitude. We excluded all the block groups reporting zero entries for the independent and dependent variables. The final data contained 20,640 observations on 9 variables. The dependent variable is ln(median house value).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/537) of an [OpenML dataset](https://www.openml.org/d/537). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/537/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/537/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/537/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

